1. When t_delay is set to 0, the function async_comm_test returns only the time it takes for the MPI_Isend to return. This is a constant value at 0.0026 seconds approximately.
2. As t_delay gradually increases, the busywait function also parallelly runs alongside the MPI_Isend until the busywait return time is lesser than or equal to the MPI_Isend return time. This is represented as a nearly straight line parallel to the X-axis until about 0.0026s. Then, the time elapsed is given by t_delay increasing step-wise. This gives rise to an almost x=y line after the parallel overlap of computation and communication until 0.0026 seconds.
3.There is an overlap between communication and computation as long as the time taken for the busywait function is less than or equal to th amount of time it takes for the MPI_Isend to return. So, as stated above, the communication and computation overlap occurs until about 0.0024~0.0026 seconds, beyond which the busywait function starts eating more time than the time it takes for MPI_Isend to return.
4. Ignoring the message latency, we have T=n/bandwidth. n here corresponds to 8 mebibytes, or 8308608 bytes. T is 0.0026seconds, which is a round trip, so for a one-way trip it would be 0.0013seconds. Substituting, bandwidth is equal to about 6.45Gbps.
5. In the blocking-send implementation, there is no overlap of communication
and computation. This is because the Send does not return immediately but
waits for the message to be removed from the message buffer before moving to
the next instruction, which in this case is the computation or busy wait. As a
result the .e file reads a little different than the .e file of the
non-blocking immediate send. In the non-blocking version, the .e file would
show both processors shutting down at the same time, but in the blocking send,
there is a serial handshake policy, and only after rank 1 shuts shop is rank 0
allowed to close up. As a result there is no overlap and the graph shows
linearly increasing time proportional to the increase in t_delay steps.
6. Parallellizing the busywait and Send calls using parallel omp constructs
involve simply placing a pragma parallel and pragma single in the main just
before calling the async_comm_test function. And also a parallel task before
busy wait, and letting the master execute the MPI Send call parallelly.
Parallel task wait acts as a barrier to the two tasks and only after this
barrier does the function return. The graph for most part shows a constant
(non increasing slope) for the time period upto 0.0015seconds approximately.
This means that there is an overlap of communication and computation upto this
point. The sharp sudden increases could be due to the overhead in spawning
threads and setting up the omp framework. The step size here has been
decreased to 0.5e-4 to get a clearer resolution on the portion of the graph
that is constant.
7. The serial MPISend function requires a time of nearly 0.07 seconds to return, as
seen on the async_blocking_send graph (y-intercept) . This
means it takes about 0.035 seconds to transmit a packet. So the bandwidth
works out to 240Mbps for a message size of 8 Mebibytes. This seems very poor
in comparison to 6.45Gbps for the non blocking send as calculated in 4. The
bandwidth of the non-blocking send is around 27 times better than the bocking
send.
